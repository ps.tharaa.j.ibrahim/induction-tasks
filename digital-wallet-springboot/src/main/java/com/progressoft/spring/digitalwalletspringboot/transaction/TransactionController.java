package com.progressoft.spring.digitalwalletspringboot.transaction;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
//TODO: follow API convention(fixed)
@RequestMapping("v1/transaction")
public class TransactionController {

    private final TransferService transferService;

    private final TransactionMapper transactionMapper;

    public TransactionController(TransactionMapper transactionMapper, TransferService transferService) {
        this.transactionMapper = transactionMapper;
        this.transferService = transferService;
    }

    //TODO: convert to PostMapping and convert the others too(fixed)
    @PostMapping
    public ResponseEntity<TransactionResponse> transfer(@RequestBody TransactionRequest transactionRequest) {
        Transaction transfer = transferService.transfer(transactionRequest.getSenderAccountNumber()
                , transactionRequest.getReceiverAccountNumber(), transactionRequest.getAmount());
        return ResponseEntity.ok().body(transactionMapper.map(transfer));
    }

    @GetMapping
    public List<Transaction> findAll() {
        return transferService.findAll();
    }

    @GetMapping("/{accountNumber}")
    public List<Transaction> findAllByAccountNum(@PathVariable int accountNumber) {
        return transferService.findAll(accountNumber);
    }
}

package com.progressoft.spring.digitalwalletspringboot.wallet;

import com.progressoft.spring.digitalwalletspringboot.exception.AccountNotFoundException;
import com.progressoft.spring.digitalwalletspringboot.exception.ExceptionMessages;
import com.progressoft.spring.digitalwalletspringboot.validator.Validator;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class WalletServiceImpl implements WalletService {

    private final Validator validator;
    private final AtomicInteger accountNumber;
    private final WalletRepository walletRepository;

    public WalletServiceImpl(
            WalletRepository walletRepository,
            Validator validator) {
        this.validator = validator;
        this.walletRepository = walletRepository;
        this.accountNumber = new AtomicInteger(0);
    }

    @Override
    public Wallet create(Wallet newWallet) {
//       TODO:- add a validation to check on balance (fixed)
//        TODO:- account can initial with balance zero (fixed)
//        TODO:- you need to validate the newWallet before increment the account number (fixed)
        validateWallet(newWallet);
        throwIfExist(newWallet);
        newWallet.setAccountNumber(accountNumber.incrementAndGet());
        return walletRepository.save(newWallet);
    }

    @Override
    public Wallet findBy(int accountNumber) {
        return walletRepository.findByAccountNumber(accountNumber)
                .orElseThrow(() -> new IllegalArgumentException(ExceptionMessages.INCORRECT_ACCOUNT_NUMBER));
    }

    @Override
    public Wallet update(Wallet wallet) {
       //TODO: Please check the wallet if exist before update it
        Optional<Wallet> oldWallet = walletRepository.findByAccountNumber(wallet.getAccountNumber());
        if (oldWallet.isEmpty()) {
                throw new AccountNotFoundException("Wallet not found");
        }
        Wallet updatedWallet = oldWallet.get();
        updatedWallet.setName(wallet.getName());
        updatedWallet.setPassword(wallet.getPassword());

        return walletRepository.save(updatedWallet);
    }

    @Override
    public List<Wallet> findAll() {
        return walletRepository.findAll();
    }

    private Boolean isExist(Wallet wallet) {
        return walletRepository.existsByUserId(wallet.getUserId());
    }

    private void validateWallet(Wallet newWallet) {
        validator.checkUserId(newWallet.getUserId());
        validator.checkName(newWallet.getName());
        validator.checkPassword(newWallet.getPassword());
        validator.checkAmount(newWallet.getAmount());
    }

    private void throwIfExist(Wallet newWallet) {
        if (isExist(newWallet))
            throw new IllegalArgumentException(ExceptionMessages.EXISTED_WALLET_MSG);
    }
}

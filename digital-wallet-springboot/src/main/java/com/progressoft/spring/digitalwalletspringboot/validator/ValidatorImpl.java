package com.progressoft.spring.digitalwalletspringboot.validator;


import com.progressoft.spring.digitalwalletspringboot.exception.ExceptionMessages;
import com.progressoft.spring.digitalwalletspringboot.wallet.Wallet;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class ValidatorImpl implements Validator {
    //TODO:- Remove unused variable, and make ID_DIGIT_NUMBERS and REGEX constants (fixed)
    private final static int ID_DIGIT_NUMBERS = 10;
    public final static String USER_ID_REGEX = "[0-9]+.";

    //        TODO:- No need to do the following check:- userId.isEmpty(), remove it please.(fixed)
    @Override
    public void checkUserId(String userId) {
        if (userId == null || userId.length() != ID_DIGIT_NUMBERS || !userId.matches(USER_ID_REGEX))
            throw new IllegalArgumentException(ExceptionMessages.USERID_MSG);
    }

    //    StringUtils
    @Override
    public void checkName(String name) {
        if (name == null || name.isEmpty())
            throw new IllegalArgumentException(ExceptionMessages.USERNAME_MSG);
    }

    @Override
    public void checkPassword(String password) {
        if (password == null || password.isEmpty())
            throw new IllegalArgumentException(ExceptionMessages.PASSWORD_MSG);
    }

    @Override
    public void checkAmount(BigDecimal amount) {
        if (amount == null || amount.compareTo(BigDecimal.ZERO) == -1)
            throw new IllegalArgumentException(ExceptionMessages.INVALID_AMOUNT);
    }

    @Override
    public void checkTransactionAmount(BigDecimal amount) {
        if(amount.compareTo(BigDecimal.ZERO) <= 0 )
            throw new IllegalArgumentException(ExceptionMessages.INVALID_AMOUNT) ;
    }

    @Override
    public void checkWalletBalance(Wallet wallet, BigDecimal amount) {
        if(wallet.getAmount().compareTo(amount) == -1)
            throw new IllegalArgumentException(ExceptionMessages.INSUFFICIENT_BALANCE) ;
    }
}
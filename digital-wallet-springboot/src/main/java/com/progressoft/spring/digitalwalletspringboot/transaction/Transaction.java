package com.progressoft.spring.digitalwalletspringboot.transaction;


import com.progressoft.spring.digitalwalletspringboot.wallet.Wallet;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Transaction {
    //TODO: add ID field(fixed)
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ManyToOne
    @JoinColumn(name = "sender_acc_num", referencedColumnName = "accountNumber", nullable = false)
    private Wallet senderWallet;

    @ManyToOne
    @JoinColumn(name = "receiver_acc_num", referencedColumnName = "accountNumber", nullable = false)
    private Wallet receiverWallet;

    @Column(nullable = false)
    private BigDecimal amount;

    private Date date;

    public int getId() {
        return id;
    }

    public int getSenderAccNum() {
        return senderWallet.getAccountNumber();
    }

    public int getReceiverAccNum() {
        return receiverWallet.getAccountNumber();
    }


    public BigDecimal getAmount() {
        return amount;
    }

    public Date getDate() {
        return date;
    }


    @Override
    public String toString() {
        return "Transaction{" +
                " id=" + id +
                ", senderAccountNum=" + senderWallet.getAccountNumber() +
                ", receiverAccountNum=" + receiverWallet.getAccountNumber() +
                ", amount=" + amount +
                ", date=" + date +
                '}';
    }
}

package com.progressoft.spring.digitalwalletspringboot.wallet;

import java.math.BigDecimal;

public class WalletRequest {

    private String userId ;

    private String name ;

    private String password ;

    private BigDecimal amount ;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}

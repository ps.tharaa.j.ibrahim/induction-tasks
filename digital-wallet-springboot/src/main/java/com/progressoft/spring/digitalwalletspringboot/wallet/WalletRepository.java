package com.progressoft.spring.digitalwalletspringboot.wallet;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface WalletRepository extends CrudRepository<Wallet, Integer> {
    List<Wallet> findAll();

    Boolean existsByUserId(String userId);

    Optional<Wallet> findByAccountNumber(int accountNumber);
}
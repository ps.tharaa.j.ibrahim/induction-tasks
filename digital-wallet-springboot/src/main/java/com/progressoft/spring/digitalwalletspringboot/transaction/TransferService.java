package com.progressoft.spring.digitalwalletspringboot.transaction;


import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;

@Component
public interface TransferService {
    List<Transaction> findAll();

    List<Transaction> findAll(int accountNum);

    Transaction save(Transaction transaction);

    Transaction transfer(int senderAccNum, int receiverAccNum, BigDecimal amount);
}

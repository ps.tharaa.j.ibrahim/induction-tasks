package com.progressoft.spring.digitalwalletspringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DigitalWalletSpringboot {

	public static void main(String[] args) {
		SpringApplication.run(DigitalWalletSpringboot.class, args);
	}

}

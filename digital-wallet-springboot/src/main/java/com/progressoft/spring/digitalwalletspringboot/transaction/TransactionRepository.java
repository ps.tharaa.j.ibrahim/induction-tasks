package com.progressoft.spring.digitalwalletspringboot.transaction;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


import java.util.*;

@Repository
public interface TransactionRepository extends CrudRepository<Transaction, Integer> {
    //TODO: make it return all by sender or receiver(fixed)
    List<Transaction> findAll();

    List<Transaction> findAllBySenderWalletAccountNumber(int accountNumber);

    List<Transaction> findAllByReceiverWalletAccountNumber(int accountNumber);
}

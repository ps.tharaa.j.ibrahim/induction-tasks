package com.progressoft.spring.digitalwalletspringboot.wallet;

import org.mapstruct.Mapper;

import java.math.BigDecimal;

@Mapper (componentModel = "spring")
public class WalletResponse {

    private String name ;

    private int accountNumber ;

    private BigDecimal amount ;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(int accountNumber) {
        this.accountNumber = accountNumber;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}

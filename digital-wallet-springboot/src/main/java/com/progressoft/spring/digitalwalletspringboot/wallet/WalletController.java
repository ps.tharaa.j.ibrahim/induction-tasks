package com.progressoft.spring.digitalwalletspringboot.wallet;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("v1/wallet")
public class WalletController {
    private final WalletService walletService;
    private final WalletMapper mapper ;
    public WalletController(WalletService walletService, WalletMapper mapper) {
        this.walletService = walletService;
        this.mapper = mapper;
    }

    //TODO: convert Wallet to request and response models and provide mapper to entity(fixed)
    @PostMapping
    public ResponseEntity<WalletResponse> create(@RequestBody WalletRequest walletRequest) {
        Wallet savedWallet = walletService.create(mapper.map(walletRequest));
        WalletResponse response = new WalletResponse();
        response.setName(savedWallet.getName());
        response.setAccountNumber(savedWallet.getAccountNumber());
        response.setAmount(savedWallet.getAmount());
        return ResponseEntity.ok().body(response) ;
    }

    @GetMapping("/{accountNum}")
    public Wallet findBy(@PathVariable int accountNum) {
        return walletService.findBy(accountNum);
    }

    @PutMapping("/{accountNum}")
    public WalletResponse update(@RequestBody WalletUpdateRequest walletRequest , @PathVariable int accountNum) {

        Wallet wallet = mapper.map(walletRequest);
        wallet.setAccountNumber(accountNum);
        Wallet response = walletService.update(wallet) ;

        WalletResponse walletResponse = new WalletResponse();
        walletResponse.setName(response.getName());
        walletResponse.setAccountNumber(response.getAccountNumber());
        walletResponse.setAmount(response.getAmount()); ;

        return walletResponse  ;
    }

    @GetMapping
    public List<Wallet> findAll() {
        return walletService.findAll();
    }


}

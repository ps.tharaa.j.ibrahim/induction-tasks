package com.progressoft.spring.digitalwalletspringboot.validator;


import com.progressoft.spring.digitalwalletspringboot.wallet.Wallet;

import java.math.BigDecimal;

public interface Validator {

    void checkUserId(String userId);
    void checkName(String name) ;

    void checkPassword(String password) ;

    void checkAmount(BigDecimal amount) ;

    void checkTransactionAmount(BigDecimal amount);

    void checkWalletBalance(Wallet wallet , BigDecimal amount);



}

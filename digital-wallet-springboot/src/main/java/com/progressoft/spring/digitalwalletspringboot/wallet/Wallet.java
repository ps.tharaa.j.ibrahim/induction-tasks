package com.progressoft.spring.digitalwalletspringboot.wallet;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
@Entity
public class Wallet {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int accountNumber;
    @Column(unique = true, nullable = false)
    private String userId;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private String password;
    @Column(nullable = false)
    private BigDecimal amount;

    public Wallet(String userId, String name, String password, BigDecimal amount) {
        this.userId = userId;
        this.name = name;
        this.password = password;
        this.amount = amount;
    }

    public String getUserId() {
        return userId;
    }

    public int getAccountNumber() {
        return accountNumber;
    }


    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

//    TODO:- Remove unnecessary (unused) methods (fixed)

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (getClass() != o.getClass()) return false;
        Wallet wallet = (Wallet) o;
        return this.getUserId().equals(wallet.getUserId()) || this.getAccountNumber() == wallet.getAccountNumber();
    }

    public void setAccountNumber(int accountNumber) {
        this.accountNumber = accountNumber;

    }

    @Override
    public String toString() {
        return "Balance for account number " + accountNumber + " : " + amount;
    }
}

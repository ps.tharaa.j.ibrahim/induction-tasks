package com.progressoft.spring.digitalwalletspringboot.wallet;


import com.progressoft.spring.digitalwalletspringboot.transaction.Transaction;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public interface WalletService {
    List<Wallet> findAll();

    Wallet create(Wallet wallet);

    Wallet update(Wallet wallet);

    Wallet findBy(int accountNumber);
}

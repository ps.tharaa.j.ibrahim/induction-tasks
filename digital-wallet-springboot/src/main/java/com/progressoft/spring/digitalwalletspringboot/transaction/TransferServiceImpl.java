package com.progressoft.spring.digitalwalletspringboot.transaction;

import com.progressoft.spring.digitalwalletspringboot.validator.Validator;
import com.progressoft.spring.digitalwalletspringboot.wallet.Wallet;
import com.progressoft.spring.digitalwalletspringboot.wallet.WalletService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class TransferServiceImpl implements TransferService {
    private final AtomicInteger id;
    private final Validator validator;
    private final WalletService walletService;
    private final TransactionRepository transactionRepository;

    public TransferServiceImpl(
            WalletService walletService,
            TransactionRepository transactionRepository,
            Validator validator) {
        this.walletService = walletService;
        this.transactionRepository = transactionRepository;
        this.validator = validator;
        this.id = new AtomicInteger(0);
    }

    @Override
    public Transaction transfer(int senderAccNum, int receiverAccNum, BigDecimal amount) {
//       TODO:- check on the amount it should be greater than 0 (fixed)
        Wallet senderWallet = walletService.findBy(senderAccNum);
        Wallet receiverWallet = walletService.findBy(receiverAccNum);
        doValidation(senderWallet, amount);
        updateAmounts(senderWallet, receiverWallet, amount);
        return transactionRepository.save(
                new Transaction(id.incrementAndGet(),
                        senderWallet, receiverWallet, amount, new Date())
        );
    }

    @Override
    public List<Transaction> findAll(int accountNum) {
        List<Transaction> listOfAll = new ArrayList<>();
        transactionRepository.findAllBySenderWalletAccountNumber(accountNum).forEach(listOfAll::add);
        transactionRepository.findAllByReceiverWalletAccountNumber(accountNum).forEach(listOfAll::add);
        return listOfAll;
    }

    @Override
    public List<Transaction> findAll() {
        return transactionRepository.findAll();
    }

    @Override
    public Transaction save(Transaction transaction) {
        return transactionRepository.save(transaction);
    }

    private void updateAmounts(Wallet senderWallet, Wallet receiverWallet, BigDecimal amount) {
        senderWallet.setAmount(senderWallet.getAmount().subtract(amount));
        receiverWallet.setAmount(receiverWallet.getAmount().add(amount));
        walletService.update(senderWallet);
        walletService.update(receiverWallet);
    }

    private void doValidation(Wallet senderWallet, BigDecimal amount) {
        validator.checkTransactionAmount(amount);
        validator.checkWalletBalance(senderWallet, amount);
    }
}

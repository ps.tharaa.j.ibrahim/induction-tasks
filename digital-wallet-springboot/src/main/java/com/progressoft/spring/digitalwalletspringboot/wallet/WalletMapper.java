package com.progressoft.spring.digitalwalletspringboot.wallet;

import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface WalletMapper {
    Wallet map(WalletRequest walletRequest) ;

    Wallet map(WalletResponse walletResponse) ;

    Wallet map(WalletUpdateRequest walletRequest);
}

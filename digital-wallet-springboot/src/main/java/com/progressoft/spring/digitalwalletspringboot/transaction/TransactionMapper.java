package com.progressoft.spring.digitalwalletspringboot.transaction;

import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface TransactionMapper {
    default TransactionResponse map(Transaction transaction) {
        TransactionResponse transactionResponse = new TransactionResponse();
        transactionResponse.setAmount(transaction.getAmount());
        transactionResponse.setDate(transaction.getDate());
        transactionResponse.setSenderAccountNumber(transaction.getSenderAccNum());
        transactionResponse.setReceiverAccountNumber(transaction.getReceiverAccNum());
        return transactionResponse;
    }
}

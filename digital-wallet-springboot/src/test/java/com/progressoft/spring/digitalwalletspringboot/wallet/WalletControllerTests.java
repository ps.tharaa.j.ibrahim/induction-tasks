package com.progressoft.spring.digitalwalletspringboot.wallet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.progressoft.spring.digitalwalletspringboot.transaction.Transaction;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class WalletControllerTests {

    @MockBean
    private WalletService walletService ;
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;


    private String uri = "/v1/wallet" ;
    @Test
    void givenExistedAccountNumber_whenFindByAccountNumber_thenReturnOkStatus() throws Exception {
        Wallet wallet =  new Wallet("1111842222" ,"tharaa" , "123" , new BigDecimal(700));
        wallet.setAccountNumber(3);
        when(walletService.findBy(wallet.getAccountNumber())).thenReturn(wallet) ;

        mockMvc.perform(MockMvcRequestBuilders.get(uri+"/" + wallet.getAccountNumber())
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.userId").value(wallet.getUserId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(wallet.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.password").value(wallet.getPassword()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.amount").value(wallet.getAmount()));
    }


    @Test
    void  givenWallet_whenCreate_thenReturnSavedWallet() throws Exception {
        Wallet wallet =  new Wallet("1111842222" ,"tharaa" , "123" , new BigDecimal(700));

        WalletRequest request =  new WalletRequest();
        request.setName("tharaa");
        request.setUserId("1111842222");
        request.setAmount(new BigDecimal(700));
        request.setPassword("0123");

        when(walletService.create(wallet)).thenReturn(wallet) ;

        mockMvc.perform(MockMvcRequestBuilders.post(uri) .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request)))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.accountNumber").value(wallet.getAccountNumber()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(wallet.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.amount").value(wallet.getAmount()));
    }


    @Test
    void givenListOfWallet_whenFindAll_thenReturnWalletsList() throws Exception {

        List<Wallet> walletsList = new ArrayList<>();
        walletsList.add( new Wallet("1111842222" ,"tharaa" , "123" , new BigDecimal(700)));
        walletsList.add( new Wallet("1111849222" ,"jamil" , "123" , new BigDecimal(700)));
        when(walletService.findAll()).thenReturn(walletsList);

        mockMvc.perform(MockMvcRequestBuilders.get(uri)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.size()").value(walletsList.size())) ;

    }

    @Test
    void givenExistedWallet_whenUpdateWallet_thenReturnOKStatus() throws Exception {
        Wallet wallet = new Wallet("1111842222" , "tharaa" , "123" , new BigDecimal(700)) ;
        wallet.setAccountNumber(1);

        WalletUpdateRequest updateRequest = new WalletUpdateRequest() ;
        updateRequest.setName("tharaa jamil");
        updateRequest.setPassword("052sss");

        Wallet updatedWallet = new Wallet("1111842222" , "tharaa jamil" , "052sss" , new BigDecimal(700)) ;
        updatedWallet.setAccountNumber(1);

        when(walletService.update(wallet)).thenReturn(updatedWallet);

       mockMvc.perform(MockMvcRequestBuilders.put(uri + "/" + wallet.getAccountNumber())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateRequest)))
                .andExpect(status().isOk())
               .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(updatedWallet.getName()))
               .andExpect(MockMvcResultMatchers.jsonPath("$.accountNumber").value(updatedWallet.getAccountNumber()))
               .andExpect(MockMvcResultMatchers.jsonPath("$.amount").value(updatedWallet.getAmount()));

    }


}

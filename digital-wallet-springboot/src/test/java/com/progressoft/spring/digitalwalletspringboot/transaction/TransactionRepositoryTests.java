package com.progressoft.spring.digitalwalletspringboot.transaction;

import com.progressoft.spring.digitalwalletspringboot.wallet.Wallet;
import com.progressoft.spring.digitalwalletspringboot.wallet.WalletRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.InvalidDataAccessApiUsageException;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

@DataJpaTest
public class TransactionRepositoryTests {

    @Autowired
    private TransactionRepository transactionRepository;
    @Autowired
    private WalletRepository walletRepository;

    @Test
    void givenNullWallet_whenSave_thenFail(){

        Transaction transaction = Transaction.builder()
                .senderWallet(null)
                .receiverWallet(null)
                .amount(new BigDecimal(500))
                .build();
        Assertions.assertThrows(DataIntegrityViolationException.class, () -> transactionRepository.save(transaction)) ;

    }

    @Test
    void givenNullAmount_whenSave_thenFail(){

        Transaction transaction = Transaction.builder()
                .senderWallet(new Wallet())
                .receiverWallet(new Wallet())
                .amount(null)
                .build();
        Assertions.assertThrows(InvalidDataAccessApiUsageException.class, () -> transactionRepository.save(transaction)) ;

    }


    @Test
    void givenTransactions_whenFindAll_thenReturnAllTransactions() {
     BigDecimal amount = new BigDecimal(100) ;

        Wallet senderWallet = new Wallet("1111842222" ,"tharaa" , "123" , new BigDecimal(700));
        walletRepository.save(senderWallet);

        Wallet receiverWallet = new Wallet("1111822222" ,"jamil" , "123" , new BigDecimal(100) );
        walletRepository.save(receiverWallet);

        Transaction transaction = Transaction.builder()
                .senderWallet(senderWallet)
                .receiverWallet(receiverWallet)
                .amount(amount)
                .build();
        List<Transaction> expectedList = Arrays.asList(transaction);

        transactionRepository.save(transaction) ;
        Assertions.assertEquals(transactionRepository.findAll() , expectedList);
    }
}

package com.progressoft.spring.digitalwalletspringboot.validator;
import com.progressoft.spring.digitalwalletspringboot.exception.ExceptionMessages;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

public class ValidatorTests {

    private final Validator validator  = new ValidatorImpl() ;


    @Test
    void givenNullUserData_whenValidate_thenReturnIllegalArgumentException(){

        IllegalArgumentException idException = Assertions.assertThrows(IllegalArgumentException.class , ()-> validator.checkUserId(null)) ;
        IllegalArgumentException nameException = Assertions.assertThrows(IllegalArgumentException.class , ()-> validator.checkName(null)) ;
        IllegalArgumentException passwordException = Assertions.assertThrows(IllegalArgumentException.class , ()-> validator.checkPassword(null)) ;
        IllegalArgumentException amountException = Assertions.assertThrows(IllegalArgumentException.class , ()-> validator.checkAmount(null)) ;

        Assertions.assertEquals(ExceptionMessages.USERID_MSG, idException.getMessage());
        Assertions.assertEquals(ExceptionMessages.USERNAME_MSG , nameException.getMessage());
        Assertions.assertEquals(ExceptionMessages.PASSWORD_MSG , passwordException.getMessage());
        Assertions.assertEquals(ExceptionMessages.INVALID_AMOUNT, amountException.getMessage());


    }

    @Test
    void givenEmptyUserData_whenValidate_thenReturnIllegalArgumentException(){

        IllegalArgumentException idException = Assertions.assertThrows(IllegalArgumentException.class , ()-> validator.checkUserId("")) ;
        IllegalArgumentException nameException = Assertions.assertThrows(IllegalArgumentException.class , ()-> validator.checkName("")) ;
        IllegalArgumentException passwordException = Assertions.assertThrows(IllegalArgumentException.class , ()-> validator.checkPassword("")) ;
        IllegalArgumentException amountException = Assertions.assertThrows(IllegalArgumentException.class , ()-> validator.checkAmount(null)) ;

        Assertions.assertEquals(ExceptionMessages.USERID_MSG, idException.getMessage());
        Assertions.assertEquals(ExceptionMessages.USERNAME_MSG , nameException.getMessage());
        Assertions.assertEquals(ExceptionMessages.PASSWORD_MSG , passwordException.getMessage());
        Assertions.assertEquals(ExceptionMessages.INVALID_AMOUNT, amountException.getMessage());

    }

    @Test
    void givenUserIdLessThanTenLength_whenCheckUserId_thenThrowIllegalArgumentException(){
        IllegalArgumentException idException = Assertions.assertThrows(IllegalArgumentException.class , ()-> validator.checkUserId("012345")) ;
        Assertions.assertEquals(ExceptionMessages.USERID_MSG , idException.getMessage());
    }

    @Test
    void givenUserIdContainsCharactersOrSymbols_whenCheckUserId_thenThrowIllegalArgumentException(){
        IllegalArgumentException idException = Assertions.assertThrows(IllegalArgumentException.class ,
                ()-> validator.checkUserId("d12345678*")) ;
        IllegalArgumentException idException2 = Assertions.assertThrows(IllegalArgumentException.class ,
                ()-> validator.checkUserId("012d45678*")) ;
        Assertions.assertEquals(ExceptionMessages.USERID_MSG , idException.getMessage());
        Assertions.assertEquals(ExceptionMessages.USERID_MSG , idException2.getMessage());
    }

    @Test
    void givenAmountLessThanZero_whenValidate_thenThrowIllegalArgumentException(){
        IllegalArgumentException amountException = Assertions.assertThrows(IllegalArgumentException.class ,
                ()-> validator.checkAmount(new BigDecimal(-500))) ;
        Assertions.assertEquals(ExceptionMessages.INVALID_AMOUNT, amountException.getMessage());
    }

    @Test
    void givenAmountLessThanZero_whenTransferWithInsufficientBalance_thenThrowIllegalArgumentsException(){
        IllegalArgumentException amountException = Assertions.assertThrows(IllegalArgumentException.class ,
                ()-> validator.checkTransactionAmount(new BigDecimal(-500))) ;
        Assertions.assertEquals(ExceptionMessages.INVALID_AMOUNT, amountException.getMessage());
    }

    @Test
    void givenAmountEqualsZero_whenTransferWithAmountEqualZero_thenThrowIllegalArgumentsException(){
        IllegalArgumentException amountException = Assertions.assertThrows(IllegalArgumentException.class ,
                ()-> validator.checkTransactionAmount(new BigDecimal(0))) ;
        Assertions.assertEquals(ExceptionMessages.INVALID_AMOUNT, amountException.getMessage());
    }




}

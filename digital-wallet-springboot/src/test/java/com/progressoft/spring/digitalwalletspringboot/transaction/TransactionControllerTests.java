package com.progressoft.spring.digitalwalletspringboot.transaction;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.progressoft.spring.digitalwalletspringboot.wallet.Wallet;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class TransactionControllerTests {
    @MockBean
    TransferService transferService;
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;


    @Test
    void givenListOfTransaction_whenFindAll_returnOkStatus() throws Exception {

        String uri = "/v1/transaction";
        List<Transaction> transactionsList = new ArrayList<>();
        transactionsList.add(new Transaction(1, new Wallet(), new Wallet(), new BigDecimal(100), new Date()));
        transactionsList.add(new Transaction(2, new Wallet(), new Wallet(), new BigDecimal(500), new Date()));

        when(transferService.findAll()).thenReturn(transactionsList);

        mockMvc.perform(MockMvcRequestBuilders.get(uri)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.size()").value(transactionsList.size()));

    }

    @Test
    void givenListOfTransaction_whenFindAllByAccountNumber_thenReturnAllTransactionsThatHaveTheAccountNumber() throws Exception {
        String uri = "/v1/transaction/2";
        List<Transaction> expectedList = new ArrayList<>();

        Wallet wallet1 = new Wallet();
        wallet1.setAccountNumber(2);

        List<Transaction> transactionsList = new ArrayList<>();
        transactionsList.add(new Transaction(1, wallet1, new Wallet(), new BigDecimal(100), new Date()));
        transactionsList.add(new Transaction(2, new Wallet(), wallet1, new BigDecimal(500), new Date()));
        transactionsList.add(new Transaction(2, new Wallet(), new Wallet(), new BigDecimal(500), new Date()));

        for (Transaction transaction : transactionsList) {
            if (transaction.getReceiverAccNum() == wallet1.getAccountNumber()
                    || transaction.getSenderAccNum() == wallet1.getAccountNumber())
                expectedList.add(transaction);
        }

        when(transferService.findAll(2)).thenReturn(expectedList);


        mockMvc.perform(MockMvcRequestBuilders.get(uri)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.size()").value(expectedList.size()));

    }

    @Test
    void givenSenderAndReceiverAccountNumber_whenTransfer_thenReturnTransaction() throws Exception {
        String uri = "/v1/transaction";

        TransactionRequest request = new TransactionRequest();
        request.setSenderAccountNumber(123);
        request.setReceiverAccountNumber(456);
        request.setAmount(BigDecimal.valueOf(2000));

        Wallet senderWallet = new Wallet();
        senderWallet.setAccountNumber(123);

        Wallet receiverWallet = new Wallet();
        receiverWallet.setAccountNumber(456);

        Transaction transaction = Transaction.builder().senderWallet(senderWallet)
                .receiverWallet(receiverWallet).build();

        when(transferService.transfer(request.getSenderAccountNumber(), request.getReceiverAccountNumber(),
                request.getAmount())).thenReturn(transaction);

        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                        .content(objectMapper.writeValueAsString(request))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.senderAccountNumber").value(transaction.getSenderAccNum()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.receiverAccountNumber").value(transaction.getReceiverAccNum()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.date").value(transaction.getDate()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.amount").value(transaction.getAmount()));
    }
}

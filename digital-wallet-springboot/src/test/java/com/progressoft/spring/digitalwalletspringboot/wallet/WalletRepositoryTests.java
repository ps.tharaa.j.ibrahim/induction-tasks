package com.progressoft.spring.digitalwalletspringboot.wallet;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.JdbcTemplate;


import javax.persistence.EntityManager;
import javax.sql.DataSource;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
public class WalletRepositoryTests {

    @Autowired private DataSource dataSource;
    @Autowired private JdbcTemplate jdbcTemplate;
    @Autowired private EntityManager entityManager;
    @Autowired private WalletRepository walletRepository;



    @Test
    void injectedComponentsAreNotNull(){
        assertThat(dataSource).isNotNull();
        assertThat(jdbcTemplate).isNotNull();
        assertThat(entityManager).isNotNull();
        assertThat(walletRepository).isNotNull();
    }

    @Test
    void givenNullIdNumber_whenCreateWallet_thenFail(){
        Wallet wallet =  new Wallet(null ,"tharaa" , "123" , new BigDecimal(700));

        Assertions.assertThrows(DataIntegrityViolationException.class, () -> walletRepository.save(wallet)) ;

    }

    @Test
    void givenNullName_whenCreateWallet_thenFail(){
        Wallet wallet =  new Wallet("1111842222" ,null , "123" , new BigDecimal(700));

        Assertions.assertThrows(DataIntegrityViolationException.class, () -> walletRepository.save(wallet)) ;

    }

    @Test
    void givenNullPassword_whenCreateWallet_thenFail(){
        Wallet wallet = new Wallet("1111842222" ,"tharaa" , null , new BigDecimal(700));

        Assertions.assertThrows(DataIntegrityViolationException.class, () -> walletRepository.save(wallet)) ;

    }

    @Test
    void givenNullAmount_whenCreateWallet_thenFail(){
        Wallet wallet = new Wallet("1111842222" ,"tharaa" , "123" ,null);

        Assertions.assertThrows(DataIntegrityViolationException.class, () -> walletRepository.save(wallet)) ;

    }

    @Test
    void givenWallet_whenSave_thenExistsById(){
        Wallet wallet =  new Wallet("1111842222" ,"tharaa" , "123" , new BigDecimal(700));
        walletRepository.save(wallet);

        assertThat(walletRepository.existsByUserId(wallet.getUserId())).isNotNull() ;
    }

    @Test
    void givenWallets_whenFindAll_returnAllWallets(){
        Wallet senderWallet =  new Wallet("1111842222" ,"tharaa" , "123" , new BigDecimal(700));
        walletRepository.save(senderWallet);

        Wallet receiverWallet =  new Wallet("1911842222" ,"jamil" , "123" , new BigDecimal(700));
        walletRepository.save(receiverWallet);

        List<Wallet> expectedWalletList = Arrays.asList(senderWallet , receiverWallet);
        Assertions.assertEquals(walletRepository.findAll() , expectedWalletList);


    }

    @Test
    void givenAccountNumber_whenFindByAccountNumber_thenReturnTheRequiredWallet(){
         Wallet wallet =  new Wallet("1111842222" ,"tharaa" , "123" , new BigDecimal(700));

        walletRepository.save(wallet);
        Assertions.assertEquals(walletRepository.findByAccountNumber(wallet.getAccountNumber()).get() , wallet);
    }

}

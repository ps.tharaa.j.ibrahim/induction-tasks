package com.progressoft.spring.digitalwalletspringboot.transaction;

import com.progressoft.spring.digitalwalletspringboot.exception.ExceptionMessages;
import com.progressoft.spring.digitalwalletspringboot.validator.Validator;
import com.progressoft.spring.digitalwalletspringboot.wallet.Wallet;
import com.progressoft.spring.digitalwalletspringboot.wallet.WalletRepository;
import com.progressoft.spring.digitalwalletspringboot.wallet.WalletService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.web.servlet.MockMvc;


import javax.persistence.EntityManager;
import javax.sql.DataSource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


@SpringBootTest
@AutoConfigureMockMvc
public class TransferServiceTests {

    @MockBean
    private WalletRepository walletRepository;
    private WalletService walletService = mock(WalletService.class);
    private TransactionRepository transactionRepository = mock(TransactionRepository.class);
    private TransferService transferService =
            new TransferServiceImpl(walletService, transactionRepository, mock(Validator.class));

    @Autowired
    MockMvc mockMvc;
    @Autowired
    private DataSource dataSource;
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private EntityManager entityManager;

    @Test
    void injectedComponentsAreNotNull() {
        assertThat(dataSource).isNotNull();
        assertThat(jdbcTemplate).isNotNull();
        assertThat(entityManager).isNotNull();
        assertThat(walletRepository).isNotNull();
    }

    @Test
    void givenTwoUnExistedAccounts_whenTransfer_thenThrowIllegalArgumentsException() {
        when(walletService.findBy(anyInt())).thenThrow(new IllegalArgumentException(ExceptionMessages.INCORRECT_ACCOUNT_NUMBER));
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> transferService.transfer(5, 10, new BigDecimal(300)));
        Assertions.assertEquals(ExceptionMessages.INCORRECT_ACCOUNT_NUMBER, exception.getMessage());
    }

    @Test
    void givenTwoExistedAccounts_whenTransferWithSufficientBalance_thenTransferIsSuccessful() {
        BigDecimal transferAmount = new BigDecimal(10);
        int senderAccNum = 1;
        int receiverAccNum = 2;
        Wallet senderWallet = new Wallet();
        senderWallet.setAmount(BigDecimal.TEN);
        Wallet receiverWallet = new Wallet();
        receiverWallet.setAmount(BigDecimal.ONE);
        Transaction expectedTx = Transaction.builder()
                .id(10)
                .senderWallet(senderWallet)
                .receiverWallet(receiverWallet)
                .amount(transferAmount)
                .build();

        when(walletService.findBy(senderAccNum)).thenReturn(senderWallet);
        when(walletService.findBy(receiverAccNum)).thenReturn(receiverWallet);
        when(transactionRepository.save(any(Transaction.class))).thenReturn(expectedTx);

        Transaction transaction = transferService.transfer(senderAccNum, receiverAccNum, transferAmount);
        assertTrue(transaction.getId() != 0);
        assertEquals(0, senderWallet.getAmount().compareTo(BigDecimal.ZERO));
        assertEquals(0, receiverWallet.getAmount().compareTo(BigDecimal.valueOf(11)));
    }

    @Test
    void givenListOfTransactions_whenFindAll_thenReturnAllTransactions(){
        List<Transaction> transactions = new ArrayList<>();
        transactions.add(new Transaction(1 , new Wallet() , new Wallet() , new BigDecimal(500) , new Date() )) ;
        transactions.add(new Transaction(2 , new Wallet() , new Wallet() , new BigDecimal(500) , new Date() )) ;
        transactions.add(new Transaction(3 , new Wallet() , new Wallet() , new BigDecimal(500) , new Date() )) ;

        when(transactionRepository.findAll()).thenReturn(transactions) ;

        assertEquals(transactions , transferService.findAll() );
    }


    @Test
    void givenNewTransaction_whenSave_then(){
       Transaction transaction = new Transaction(1 , new Wallet() , new Wallet() , new BigDecimal(500) , new Date() ) ;

        when(transactionRepository.save(transaction)).thenReturn(transaction);

        assertEquals(transferService.save(transaction) , transaction);
    }
}

package com.progressoft.spring.digitalwalletspringboot.wallet;


import com.progressoft.spring.digitalwalletspringboot.exception.ExceptionMessages;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;


import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;

@SpringBootTest
@AutoConfigureMockMvc
public class WalletServiceTests {

    @MockBean
    private WalletRepository walletRepository;
    @Autowired
    private WalletServiceImpl walletService;

    @Test
    void givenNewValidWallet_whenCreateWallet_thenCreateNewWallet() {
        Wallet wallet = new Wallet("0123456789", "tharaa", "123", new BigDecimal(100));
        when(walletRepository.save(any(Wallet.class))).thenReturn(wallet);
        Assertions.assertEquals(walletService.create(wallet).getUserId(), wallet.getUserId());
    }

    @Test
    void givenWalletWithWrongUserId_whenCreateWallet_thenThrowIllegalArgumentException() {
        Wallet wallet = new Wallet("012345", "tharaa", "123", new BigDecimal(100));
        when(walletRepository.save(any(Wallet.class))).thenReturn(wallet);
       IllegalArgumentException exception =  Assertions.assertThrows(IllegalArgumentException.class ,
               () -> walletService.create(wallet) ) ;
        Assertions.assertEquals(ExceptionMessages.USERID_MSG , exception.getMessage() );
    }

    @Test
    void givenWalletWithAmountLessThanZero_whenCreateWallet_thenThrowIllegalArgumentException() {
        Wallet wallet = new Wallet("0123456789", "tharaa", "123", new BigDecimal(-100));
        when(walletRepository.save(any(Wallet.class))).thenReturn(wallet);
        IllegalArgumentException exception =  Assertions.assertThrows(IllegalArgumentException.class ,
                () -> walletService.create(wallet) ) ;
        Assertions.assertEquals(ExceptionMessages.INVALID_AMOUNT, exception.getMessage() );
    }

    @Test
    void givenWalletWithAmountEqualZero_whenCreateWallet_thenCreateNewWallet() {
        Wallet wallet = new Wallet("0123456789", "tharaa", "123", new BigDecimal(0));
        when(walletRepository.save(any(Wallet.class))).thenReturn(wallet);
        Assertions.assertEquals(walletService.create(wallet).getUserId(), wallet.getUserId());
    }

    @Test
    void givenExistedWallet_whenCreateWallet_thenThrowIllegalArgumentException() {
        Wallet wallet = new Wallet("0123456789", "tharaa", "123", new BigDecimal(100));
        when(walletRepository.existsByUserId(wallet.getUserId())).thenReturn(true);
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> walletService.create(wallet));
        Assertions.assertEquals(ExceptionMessages.EXISTED_WALLET_MSG, exception.getMessage());
    }

    @Disabled
    @Test
    void givenWallets_whenOverview_thenReturnTheAmountOfEachWallet() {
        Wallet wallet = new Wallet("0123456789", "tharaa", "123", new BigDecimal(200));
        walletService.create(wallet);
        Wallet wallet1 = new Wallet("0123456788", "tharaa", "123", new BigDecimal(300));
        walletService.create(wallet1);
        Wallet wallet2 = new Wallet("0123456787", "tharaa", "123", new BigDecimal(100));
        walletService.create(wallet2);
        List<String> expectedOutput = Arrays.asList(wallet.toString(), wallet1.toString(), wallet2.toString());
        Assertions.assertEquals(expectedOutput.toString(), walletRepository.findAll().toString());
    }
}

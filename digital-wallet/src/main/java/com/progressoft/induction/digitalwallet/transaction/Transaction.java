package com.progressoft.induction.digitalwallet.transaction;

import java.math.BigDecimal;
import java.util.Date;

public class Transaction {
    //TODO: add ID field(fixed)
    private  final int id ;
    private final int fromAccountNum ;
    private final int toAccountNum ;
    private final BigDecimal amount ;
    private final Date date ;

    public Transaction(int id , int fromAccountNum, int toAccountNum, BigDecimal amount, Date date) {
        this.id = id ;
        this.fromAccountNum = fromAccountNum;
        this.toAccountNum = toAccountNum;
        this.amount = amount;
        this.date = date;
    }

    public int getFromAccountNum() {
        return fromAccountNum;
    }

    public int getToAccountNum() {
        return toAccountNum;
    }


    public BigDecimal getAmount() {
        return amount;
    }


    @Override
    public String toString() {
        return "Transaction{" +
                "fromAccountNum=" + fromAccountNum +
                ", toAccountNum=" + toAccountNum +
                ", amount=" + amount +
                ", date=" + date +
                '}';
    }
}

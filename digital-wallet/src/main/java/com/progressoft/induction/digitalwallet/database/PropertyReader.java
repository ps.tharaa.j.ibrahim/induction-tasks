package com.progressoft.induction.digitalwallet.database;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyReader {

    public static Properties getPropertyReader(String filename){

        try (InputStream input = PropertyReader.class.getClassLoader().getResourceAsStream(filename)) {
            Properties property = new Properties() ;
            property.load(input);
            return property ;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

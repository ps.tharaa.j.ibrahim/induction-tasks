package com.progressoft.induction.digitalwallet.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;


public class DatabaseConnection {
   private static DatabaseConnection instance;
   private static Connection connection ;
   private DatabaseConnection(){
      Properties propertyReader = PropertyReader.getPropertyReader("connection.properties" );
      try{
         Class.forName(propertyReader.getProperty("driverName"));
         connection = DriverManager.getConnection(propertyReader.getProperty("url") , propertyReader.getProperty("username")
                 , propertyReader.getProperty("password"));

      }catch (SQLException | ClassNotFoundException e){
         try {
            connection.close();
         } catch (SQLException ex) {
            throw new RuntimeException(ex);
         }
         throw new RuntimeException(e) ;
      }
   }
   public Connection getConnection() {
      return connection;
   }
   public static DatabaseConnection getInstance() {

      try {
         if (instance == null || instance.getConnection().isClosed()) {
            instance = new DatabaseConnection();
         }
      } catch (SQLException e) {
         throw new RuntimeException(e);
      }
      return instance;
   }
}

package com.progressoft.induction.digitalwallet.transaction;

import com.progressoft.induction.digitalwallet.exception.ExceptionMessages;
import com.progressoft.induction.digitalwallet.validator.Validator;
import com.progressoft.induction.digitalwallet.wallet.Wallet;
import com.progressoft.induction.digitalwallet.wallet.WalletService;

import java.math.BigDecimal;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

public class TransferServiceImpl implements TransferService{
    private final WalletService walletService ;
    private final TransactionRepository transactionHistory ;

    private final Validator validator ;

    private final AtomicInteger id;

    public TransferServiceImpl(WalletService walletService , Validator validator, TransactionRepository transactionRepository) {
        this.walletService = walletService ;
        this.validator = validator ;
        this.transactionHistory = transactionRepository ;
        this.id = new AtomicInteger(0);
    }

    @Override
    public void transfer(int senderAccNum, int receiverAccNum, BigDecimal amount) {
//       TODO:- check on the amount it should be greater than 0 (fixed)
        validator.checkTransactionAmount(amount);
        Wallet senderWallet = walletService.findBy(senderAccNum) ;
        Wallet receiverWallet = walletService.findBy(receiverAccNum) ;
        validator.checkWalletBalance(senderWallet , amount);
        BigDecimal newSenderAmount = senderWallet.getAmount().subtract(amount) ;
        BigDecimal newReceiverAmount = receiverWallet.getAmount().add(amount);
        senderWallet.setAmount(newSenderAmount);
        receiverWallet.setAmount(newReceiverAmount);
        walletService.update(senderWallet);
        walletService.update(receiverWallet);
        transactionHistory.save(new Transaction(id.incrementAndGet() ,senderAccNum , receiverAccNum , amount , new Date()));
    }

}

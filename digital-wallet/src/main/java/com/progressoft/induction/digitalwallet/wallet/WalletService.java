package com.progressoft.induction.digitalwallet.wallet;
import java.util.* ;

public interface WalletService {
    int create(Wallet wallet);
    Wallet findBy(int accountNumber) ;

    void update (Wallet wallet) ;
    List<Wallet> findAll();
}

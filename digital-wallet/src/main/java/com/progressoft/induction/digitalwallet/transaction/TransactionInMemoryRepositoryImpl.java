package com.progressoft.induction.digitalwallet.transaction;

import com.progressoft.induction.digitalwallet.exception.ExceptionMessages;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TransactionInMemoryRepositoryImpl implements TransactionRepository {

    List<Transaction> transactions = new ArrayList<>();
    @Override
    public void save(Transaction transaction) {
           transactions.add(transaction) ;
    }

    @Override
    public List<Transaction> findAllBy(int accountNumber) {
        List<Transaction> historyTransactions = transactions.stream().filter(transaction -> (transaction.getFromAccountNum() == accountNumber || transaction.getToAccountNum() == accountNumber))
                .collect(Collectors.toList()) ;
        if(historyTransactions.isEmpty()) throw new IllegalArgumentException(ExceptionMessages.INCORRECT_ACCOUNT_NUMBER) ;
        return historyTransactions ;
    }
}

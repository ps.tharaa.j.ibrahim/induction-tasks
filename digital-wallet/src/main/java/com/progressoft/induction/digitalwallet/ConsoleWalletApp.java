package com.progressoft.induction.digitalwallet;

import com.progressoft.induction.digitalwallet.database.DatabaseConnection;
import com.progressoft.induction.digitalwallet.exception.ExceptionMessages;
import com.progressoft.induction.digitalwallet.transaction.*;
import com.progressoft.induction.digitalwallet.validator.Validator;
import com.progressoft.induction.digitalwallet.wallet.*;
import com.progressoft.induction.digitalwallet.validator.ValidatorImpl;


import java.math.BigDecimal;
import java.util.Scanner;

public class ConsoleWalletApp {

    //    TODO:- add tests for the main class (you can use mock from mockito dependency to mock the Scanner)
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        WalletRepository walletRepository = new WalletPersistentRepositoryImpl();
        TransactionRepository transactionRepository = new TransactionPersistentRepository() ;
        Validator validator = new ValidatorImpl() ;

        WalletService walletService = new WalletServiceImpl(validator, walletRepository);
        while (true) {
            System.out.println("Enter number of options:\n" +
                    "1-Create wallet\n" +
                    "2-Transfer\n" + "3-Overview\n" + "4-Account Statement\n" + "5-Exit");
            while (!input.hasNextInt()){
                System.err.println("Please enter a number");
                input.next() ;
            }
            int optionNumber = input.nextInt();
//            TODO:- validate all inputs user (hasNextInt, etc..) (fixed)
            try {
                switch (optionNumber) {
                    case 1:
                        Wallet wallet = new Wallet(getUserId(input), getUserName(input), getPassword(input), getAmount(input));
                        wallet.setAccountNumber(walletService.create(wallet));
                        System.out.println("Account created for user " + wallet.getName() + " with account number " + wallet.getAccountNumber());
                        break;
                    case 2:
                        TransferService transferService = new TransferServiceImpl(walletService  ,validator , transactionRepository);
                        System.out.println("You Selected Transfer");
                        int senderAccNum = getAccountNumber(input , "Enter the SENDER account number" );
                        int receiverAccNum = getAccountNumber(input , "Enter RECEIVER account number");
                        BigDecimal amount =getAmount(input);
                        transferService.transfer(senderAccNum, receiverAccNum, amount);
                        System.out.println("Transfer Successful");
//                        TODO:- add break here (fixed)
                        break;
                    case 3:
                        System.out.println("YOU SELECTED OVERVIEW");
                        if (walletRepository.findAll().isEmpty()) System.out.println("There is no wallets yet");
                        else walletRepository.findAll().stream().forEach(System.out::println) ;
                        break;
                    case 4 :
                        System.out.println("YOU SELECTED ACCOUNT STATEMENT");
                        int accountNum = getAccountNumber(input , "Enter account number" );
                        System.out.println("Summary for account number: " + accountNum);
                        System.out.println("Current Balance: " + walletRepository.findBy(accountNum).getAmount());
                        System.out.println("Your Transaction History");
                        transactionRepository.findAllBy(accountNum).stream().forEach(System.out::println);
                        break;
                    case 5:
                        System.exit(0);
                        break;
                    default:
                        System.err.println(ExceptionMessages.OPTIONS_MSG);
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

    private static String getUserId(Scanner input) {
        System.out.println("User Id:");
        //TODO: Edit the Regex to validate the valid cases rather than the invalid cases(fixed)
        while (!input.hasNext(ValidatorImpl.USER_ID_REGEX)) {
            System.err.println("Invalid user ID");
            System.out.println("User Id:");
            input.next();
        }
        return input.next();
    }

    private static String getUserName(Scanner input) {
        System.out.println("User name:");
        return input.next();
    }

    private static String getPassword(Scanner input) {
        System.out.println("Password:");
        return input.next();
    }

    private static BigDecimal getAmount(Scanner input) {
        System.out.println("Amount:");
        while (!input.hasNextBigDecimal()) {
            System.err.println("Invalid amount:");
            System.out.println("Amount:");
            input.next();
        }
        return input.nextBigDecimal();
    }

    private static int getAccountNumber(Scanner input , String typeOfInput){
        System.out.println(typeOfInput);
        while (!input.hasNextInt()){
            System.err.println("Invalid account number");
            System.out.println(typeOfInput);
            input.next();
        }
        return input.nextInt();
    }
}

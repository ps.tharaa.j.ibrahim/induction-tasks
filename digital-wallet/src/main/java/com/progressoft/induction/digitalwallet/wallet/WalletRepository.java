package com.progressoft.induction.digitalwallet.wallet;

import java.math.BigDecimal;
import java.util.*;

public interface WalletRepository {
    int save(Wallet wallet);

    Boolean isExist(Wallet wallet);

    List<Wallet> findAll();

    Wallet findBy(int accountNumber) ;

    void update(Wallet wallet) ;
}
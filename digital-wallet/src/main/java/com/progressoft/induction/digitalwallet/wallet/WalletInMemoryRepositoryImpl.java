package com.progressoft.induction.digitalwallet.wallet;

import com.progressoft.induction.digitalwallet.exception.ExceptionMessages;

import java.math.BigDecimal;
import java.util.*;

public class WalletInMemoryRepositoryImpl implements WalletRepository {

//    TODO:- use List here (fixed)
    private final List<Wallet> wallets = new ArrayList<>();

    @Override
    public int save(Wallet wallet) {
        wallets.add(wallet);
        return wallet.getAccountNumber();
    }

    @Override
    public Boolean isExist(Wallet wallet) {
        return wallets.contains(wallet);
    }

//    TODO:- findAll (fixed)
    @Override
    public List<Wallet> findAll() {
        return wallets;
    }

//    TODO:- findBy (fixed)
    @Override
    public Wallet findBy(int accountNumber) {

        return findAll().stream().filter(wallet -> wallet.getAccountNumber() == accountNumber)
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(ExceptionMessages.INCORRECT_ACCOUNT_NUMBER)) ;

    }

    @Override
    public void update(Wallet wallet) {
        wallets.stream().filter(oldWallet -> oldWallet.getAccountNumber() == wallet.getAccountNumber())
                .findFirst()
                .map(oldWallet -> oldWallet = wallet);
    }
}

package com.progressoft.induction.digitalwallet.exception;

public class ExceptionMessages {
    //TODO:- Replace incorrect password with The password is required (fixed)
    public static final String PASSWORD_MSG = "The password is required"; ;
    public static final String USERNAME_MSG = "The user name is required";
    public static final String USERID_MSG = "User Id must be 10 digits" ;
    public static final String AMOUNT_MSG =  "Invalid amount" ;

    public static final String OPTIONS_MSG = "Invalid option number" ;

    public static final String EXISTED_WALLET_MSG = "This wallet is already exist" ;
    public static final String INCORRECT_ACCOUNT_NUMBER = "Incorrect sender account number or receiver account number" ;
    public static final String INSUFFICIENT_BALANCE = "Insufficient balance" ;

}

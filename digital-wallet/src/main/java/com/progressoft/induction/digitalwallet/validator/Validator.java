package com.progressoft.induction.digitalwallet.validator;

import com.progressoft.induction.digitalwallet.wallet.Wallet;

import java.math.BigDecimal;

public interface Validator {

    void checkUserId(String userId);
    void checkName(String name) ;

    void checkPassword(String password) ;

    void checkAmount(BigDecimal amount) ;

    void checkTransactionAmount(BigDecimal amount);

    void checkWalletBalance(Wallet wallet , BigDecimal amount);



}

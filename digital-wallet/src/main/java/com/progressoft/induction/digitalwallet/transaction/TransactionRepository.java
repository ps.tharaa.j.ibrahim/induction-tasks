package com.progressoft.induction.digitalwallet.transaction;

import java.util.*;

public interface TransactionRepository {

    void save (Transaction transaction) ;
    //TODO: change name to findAllBy(fixed)
    List<Transaction> findAllBy(int accountNum) ;

}

package com.progressoft.induction.digitalwallet.wallet;

import java.math.BigDecimal;

public class Wallet {
    private  int accountNumber;
    private final String userId ;
    private final String name ;
    private String password ;
    private BigDecimal amount ;


    public Wallet(String userId , String name, String password, BigDecimal amount) {
        this.name = name;
        this.password = password;
        this.amount = amount;
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public int getAccountNumber() {
        return accountNumber;
    }


    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

//    TODO:- Remove unnecessary (unused) methods (fixed)

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o){
        if(this == o) return true ;
        if(getClass() != o.getClass()) return false ;
        Wallet wallet = (Wallet) o ;
        return this.getUserId().equals(wallet.getUserId()) || this.getAccountNumber() == wallet.getAccountNumber() ;
    }

    public void setAccountNumber(int accountNumber) {
        this.accountNumber = accountNumber;

    }

    @Override
    public String toString() {
        return "Balance for account number " + accountNumber + " : "+ amount;
    }
}

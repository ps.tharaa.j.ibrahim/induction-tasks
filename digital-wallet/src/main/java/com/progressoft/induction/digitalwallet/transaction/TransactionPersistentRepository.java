package com.progressoft.induction.digitalwallet.transaction;

import com.progressoft.induction.digitalwallet.database.DatabaseConnection;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TransactionPersistentRepository implements TransactionRepository {


    @Override
    public void save(Transaction transaction) {
        try (Connection connection = DatabaseConnection.getInstance().getConnection() ;
             PreparedStatement stmt = connection.prepareStatement("INSERT INTO transactions(id, sender ,receiver ,amount ,date ) VALUES(?, ?, ? ,?,?)")){
            stmt.setInt(1 , 0);
            stmt.setInt(2 ,transaction.getFromAccountNum());
            stmt.setInt(3 , transaction.getToAccountNum());
            stmt.setBigDecimal(4 , transaction.getAmount());
            stmt.setDate(5 , Date.valueOf(java.time.LocalDate.now()));
            stmt.executeUpdate() ;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Transaction> findAllBy(int accountNum) {
        List<Transaction> transactionsList = new ArrayList<>();
        try(Connection connection = DatabaseConnection.getInstance().getConnection() ;
            PreparedStatement stmt =  connection.prepareStatement("SELECT * FROM transactions WHERE sender=? OR receiver=?")) {
            stmt.setInt(1 , accountNum);
            stmt.setInt(2,accountNum);
            ResultSet resultSet =  stmt.executeQuery();
            while (resultSet.next()){
                transactionsList.add(new Transaction(resultSet.getInt(1) ,resultSet.getInt(2) , resultSet.getInt(3) , resultSet.getBigDecimal(4) , resultSet.getDate(5)));
            }
            return transactionsList ;
        }catch (SQLException e) {
            throw new RuntimeException(e);
        }
        }
    }




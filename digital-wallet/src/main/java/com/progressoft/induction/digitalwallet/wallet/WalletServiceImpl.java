package com.progressoft.induction.digitalwallet.wallet;

import com.progressoft.induction.digitalwallet.exception.ExceptionMessages;
import com.progressoft.induction.digitalwallet.validator.Validator;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class WalletServiceImpl implements WalletService {

    private final Validator validator;
    private final WalletRepository walletRepository;
    private final AtomicInteger accountNumber;

    public WalletServiceImpl(Validator validator,
                             WalletRepository walletRepository) {
        this.validator = validator;
        this.walletRepository = walletRepository;
        this.accountNumber = new AtomicInteger(0);
    }

    @Override
    public int create(Wallet newWallet) {
//       TODO:- add a validation to check on balance (fixed)
//        TODO:- account can initial with balance zero (fixed)
        if (walletRepository.isExist(newWallet))
            throw new IllegalArgumentException(ExceptionMessages.EXISTED_WALLET_MSG);
//        TODO:- you need to validate the newWallet before increment the account number (fixed)
        validateWallet(newWallet);
        newWallet.setAccountNumber(accountNumber.incrementAndGet());
        return walletRepository.save(newWallet);
    }

    @Override
    public Wallet findBy(int accountNumber) {
        return walletRepository.findBy(accountNumber) ;
    }

    @Override
    public void update(Wallet wallet) {
         walletRepository.update(wallet);
    }

    @Override
    public List<Wallet> findAll() {
        return walletRepository.findAll();
    }

    private void validateWallet(Wallet newWallet) {
        validator.checkUserId(newWallet.getUserId());
        validator.checkName(newWallet.getName());
        validator.checkPassword(newWallet.getPassword());
        validator.checkAmount(newWallet.getAmount());
    }
}

package com.progressoft.induction.digitalwallet.transaction;

import java.math.BigDecimal;

public interface TransferService {

    void transfer (int senderAccNum , int receiverAccNum , BigDecimal amount) ;

}

package com.progressoft.induction.digitalwallet.wallet;

import com.progressoft.induction.digitalwallet.database.DatabaseConnection;
import com.progressoft.induction.digitalwallet.exception.ExceptionMessages;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class WalletPersistentRepositoryImpl implements WalletRepository {


    @Override
    public int save(Wallet wallet) {
        int account_number = 0;
        try (Connection connection = DatabaseConnection.getInstance().getConnection() ;
             PreparedStatement stmt = connection.prepareStatement("INSERT INTO wallets(user_id, user_name,account_number ,password , amount ) VALUES(?, ?, ? ,?,?)" , Statement.RETURN_GENERATED_KEYS);) {
            stmt.setInt(1, Integer.parseInt(wallet.getUserId()));
            stmt.setString(2, wallet.getName());
            stmt.setString(3, null);
            stmt.setString(4, wallet.getPassword());
            stmt.setBigDecimal(5, wallet.getAmount());
            stmt.executeUpdate();

            try (ResultSet rs = stmt.getGeneratedKeys()) {
                if (rs.next()) {
                    account_number = rs.getInt(1);
                }
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return account_number;
    }

    @Override
    public Boolean isExist(Wallet wallet) {
        try (Connection connection = DatabaseConnection.getInstance().getConnection() ;
             PreparedStatement stmt = connection.prepareStatement("SELECT * from wallets WHERE user_id =" + wallet.getUserId())) {
            ResultSet resultSet = stmt.executeQuery();
            return resultSet.next();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Wallet> findAll() {
        List<Wallet> walletsList = new ArrayList<>();
        try (Connection connection = DatabaseConnection.getInstance().getConnection() ;
             PreparedStatement stmt = connection.prepareStatement("SELECT * from wallets")) {
            ResultSet resultSet = stmt.executeQuery();
            while (resultSet.next()) {
                Wallet wallet = new Wallet(String.valueOf(resultSet.getInt(1)), resultSet.getString(2), resultSet.getString(4), resultSet.getBigDecimal(5));
                wallet.setAccountNumber(resultSet.getInt(3));
                walletsList.add(wallet);
            }
            return walletsList;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Wallet findBy(int accountNumber) {
        try (Connection connection = DatabaseConnection.getInstance().getConnection() ;
             PreparedStatement stmt = connection.prepareStatement("SELECT * from wallets WHERE account_number =" + accountNumber)) {
            ResultSet resultSet = stmt.executeQuery();
            while (resultSet.next()) {
                Wallet wallet = new Wallet(String.valueOf(resultSet.getInt(1)), resultSet.getString(2), resultSet.getString(4), resultSet.getBigDecimal(5));
                wallet.setAccountNumber(resultSet.getInt(3));
                return wallet;
            }
            throw new SQLException(ExceptionMessages.INCORRECT_ACCOUNT_NUMBER);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void update(Wallet wallet) {
        try (Connection connection = DatabaseConnection.getInstance().getConnection() ;
             PreparedStatement stmt = connection.prepareStatement("UPDATE wallets SET amount=? WHERE account_number=?")) {
            stmt.setBigDecimal(1 , wallet.getAmount());
            stmt.setInt(2,wallet.getAccountNumber());
          stmt.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}

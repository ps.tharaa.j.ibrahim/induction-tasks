package com.progressoft.induction.digitalwallet.wallet;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;


public class WalletInMemoryRepository {

    private final WalletRepository walletRepository = new WalletInMemoryRepositoryImpl();



    @Test
    void givenWallet_whenSave_thenAddToWalletsList() {
        Wallet wallet = new Wallet("0123456789", "tharaa", "123", new BigDecimal(100));
        walletRepository.save(wallet);
        Assertions.assertTrue(walletRepository.findAll().contains(wallet));
    }

    @Test
    void givenWalletWithZeroAmount_whenSave_thenAddToWalletList() {
        Wallet wallet = new Wallet("0123456789", "tharaa", "123", new BigDecimal(0));
        walletRepository.save(wallet);
        Assertions.assertTrue(walletRepository.isExist(wallet));
    }

    @Test
    void givenWallet_whenExist_thenReturnTrue() {
        Wallet wallet = new Wallet("0123456789", "tharaa", "123", new BigDecimal(100));
        walletRepository.save(wallet);
        Assertions.assertTrue(walletRepository.isExist(wallet));
    }

    @Test
    void givenWallet_whenGetWalletByAccountNumber_thenReturnWallet() {
        Wallet wallet = new Wallet("0123456789", "tharaa", "123", new BigDecimal(100));
        walletRepository.save(wallet);
        Wallet returnedWallet = walletRepository.findBy(wallet.getAccountNumber());
        Assertions.assertEquals(returnedWallet, wallet);
    }

}

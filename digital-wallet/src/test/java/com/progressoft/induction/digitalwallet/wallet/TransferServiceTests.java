package com.progressoft.induction.digitalwallet.wallet;

import com.progressoft.induction.digitalwallet.exception.ExceptionMessages;
import com.progressoft.induction.digitalwallet.transaction.TransactionRepository;
import com.progressoft.induction.digitalwallet.transaction.TransactionInMemoryRepositoryImpl;
import com.progressoft.induction.digitalwallet.transaction.TransferService;
import com.progressoft.induction.digitalwallet.transaction.TransferServiceImpl;
import com.progressoft.induction.digitalwallet.validator.Validator;
import com.progressoft.induction.digitalwallet.validator.ValidatorImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

public class TransferServiceTests {

    private final WalletRepository walletRepository = new WalletInMemoryRepositoryImpl();
    private final Validator validator = new ValidatorImpl() ;
    private final WalletService walletService = new WalletServiceImpl(validator,walletRepository) ;
    private final TransactionRepository transactionHistory = new TransactionInMemoryRepositoryImpl() ;
    private final TransferService transferService  = new TransferServiceImpl(walletService , validator , transactionHistory) ;



    @Test
    void givenTwoUnExistedAccounts_whenMakeValidTransfer_thenThrowIllegalArgumentsException(){
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class , ()-> transferService.transfer(1  , 2 , new BigDecimal(300))) ;
        Assertions.assertEquals(ExceptionMessages.INCORRECT_ACCOUNT_NUMBER , exception.getMessage());
    }

    @Test
    void givenTwoExistedAccounts_whenTransferWithInsufficientBalance_thenThrowIllegalArgumentsException(){
        Wallet wallet1 = new Wallet( "0123456789" , "tharaa" , "123" , new BigDecimal(100)) ;
        walletService.create(wallet1);
        Wallet wallet2 = new Wallet( "0123456788" , "tharaa" , "123" , new BigDecimal(500)) ;
        walletService.create(wallet2);

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class , ()-> transferService.transfer(wallet1.getAccountNumber()  , wallet2.getAccountNumber() , new BigDecimal(300))) ;
        Assertions.assertEquals(ExceptionMessages.INSUFFICIENT_BALANCE , exception.getMessage());
    }

    @Test
    void givenTwoExistedAccounts_whenTransferWithAmountEqualZero_thenThrowIllegalArgumentsException(){
        Wallet wallet1 = new Wallet( "0123456789" , "tharaa" , "123" , new BigDecimal(100)) ;
        walletService.create(wallet1);
        Wallet wallet2 = new Wallet( "0123456788" , "tharaa" , "123" , new BigDecimal(500)) ;
        walletService.create(wallet2);

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class , ()-> transferService.transfer(wallet1.getAccountNumber()  , wallet2.getAccountNumber() , new BigDecimal(0))) ;
        Assertions.assertEquals(ExceptionMessages.AMOUNT_MSG , exception.getMessage());
    }

    @Test
    void givenTwoExistedAccounts_whenTransferWithAmountLessThanZero_thenThrowIllegalArgumentsException(){
        Wallet wallet1 = new Wallet( "0123456789" , "tharaa" , "123" , new BigDecimal(100)) ;
        walletService.create(wallet1);
        Wallet wallet2 = new Wallet( "0123456788" , "tharaa" , "123" , new BigDecimal(500)) ;
        walletService.create(wallet2);

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class , ()-> transferService.transfer(wallet1.getAccountNumber()  , wallet2.getAccountNumber() , new BigDecimal(-1))) ;
        Assertions.assertEquals(ExceptionMessages.AMOUNT_MSG , exception.getMessage());
    }
    @Test
    void givenTwoExistedAccounts_whenTransferWithSufficientBalance_thenTransferIsSuccessful(){
        BigDecimal transferAmount = new BigDecimal(300) ;
        BigDecimal newExpectedSenderBalance = new BigDecimal(200) ;
        BigDecimal newExpectedReceiverBalance = new BigDecimal(400) ;

        Wallet wallet1 = new Wallet( "0123456789" , "tharaa" , "123" , new BigDecimal(100)) ;
        walletService.create(wallet1);
        Wallet wallet2 = new Wallet( "0123456788" , "tharaa" , "123" , new BigDecimal(500)) ;
        walletService.create(wallet2);

        transferService.transfer(wallet2.getAccountNumber()  , wallet1.getAccountNumber() , transferAmount) ;
        Assertions.assertEquals(newExpectedSenderBalance , wallet2.getAmount());
        Assertions.assertEquals(newExpectedReceiverBalance , wallet1.getAmount());
    }

}

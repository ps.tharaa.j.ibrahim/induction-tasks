package com.progressoft.induction.digitalwallet.wallet;

import com.progressoft.induction.digitalwallet.exception.ExceptionMessages;


import com.progressoft.induction.digitalwallet.transaction.Transaction;
import com.progressoft.induction.digitalwallet.transaction.TransactionRepository;
import com.progressoft.induction.digitalwallet.transaction.TransactionInMemoryRepositoryImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.*;

public class TransactionInMemoryRepositoryTests {

    private TransactionRepository transactionHistory;

    //TODO: add beforeEach to reInitialize transactionHistory(fixed)
    @BeforeEach
    void setUp(){
        transactionHistory = new TransactionInMemoryRepositoryImpl();
    }
    @Test
    void givenTransaction_whenSave_thenAddedToTransactionsList(){
        Transaction transaction1 = new Transaction(0,1 , 2 ,  new BigDecimal(100) , new Date());
        transactionHistory.save(transaction1);
        List<Transaction> expectedTransactionHistory = List.of(transaction1);
        Assertions.assertEquals(expectedTransactionHistory , transactionHistory.findAllBy(1));
        Assertions.assertEquals(expectedTransactionHistory , transactionHistory.findAllBy(2));
    }

    @Test
    void givenUnExistedAccountNumber_whenFindBy_thenThrowsIllegalArgumentException(){
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class , ()-> transactionHistory.findAllBy(5)) ;
        Assertions.assertEquals(ExceptionMessages.INCORRECT_ACCOUNT_NUMBER , exception.getMessage());
    }
}

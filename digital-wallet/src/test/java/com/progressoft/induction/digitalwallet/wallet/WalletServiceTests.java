package com.progressoft.induction.digitalwallet.wallet;

import com.progressoft.induction.digitalwallet.exception.ExceptionMessages;
import com.progressoft.induction.digitalwallet.validator.Validator;
import com.progressoft.induction.digitalwallet.validator.ValidatorImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class WalletServiceTests {

    private final Validator validator = new ValidatorImpl() ;
    private final WalletRepository walletRepository = new WalletInMemoryRepositoryImpl();
    private final WalletService walletService = new WalletServiceImpl(validator, walletRepository) ;
    private final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

    @Test
    void givenNewWallet_whenCreateWallet_thenCreateNewWallet(){
        Wallet wallet = new Wallet( "0123456789" , "tharaa" , "123" , new BigDecimal(100)) ;
        walletService.create(wallet);
        Collection<Wallet> existedWallets = walletService.findAll() ;
        Assertions.assertTrue(existedWallets.contains(wallet));
    }

    @Test
    void givenExistedWallet_whenCreateWallet_thenThrowIllegalArgumentException(){
        Wallet wallet = new Wallet( "0123456789" , "tharaa" , "123" , new BigDecimal(100)) ;
        walletService.create(wallet);
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class , ()-> walletService.create(wallet)) ;
        Assertions.assertEquals(ExceptionMessages.EXISTED_WALLET_MSG, exception.getMessage());
    }

    @Test
    void givenWallets_whenOverview_thenReturnTheAmountOfEachWallet() {
        Wallet wallet = new Wallet("0123456789", "tharaa", "123", new BigDecimal(200));
        walletService.create(wallet);
        Wallet wallet1 = new Wallet("0123456788", "tharaa", "123", new BigDecimal(300));
        walletService.create(wallet1);
        Wallet wallet2 = new Wallet("0123456787", "tharaa", "123", new BigDecimal(100));
        walletService.create(wallet2);
        List<String> expectedOutput = Arrays.asList(wallet.toString(), wallet1.toString() , wallet2.toString());
        Assertions.assertEquals(expectedOutput.toString() , walletRepository.findAll().toString());
    }
}

package com.progressoft.induction.binaryserach.binaryTree;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import static org.junit.jupiter.api.Assertions.*;


public class binaryTreeTests {

    BinaryTree binaryTree  ;
    private final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();


    @BeforeEach
    void setUp(){
        binaryTree = new BinaryTreeImpl() ;
        System.setOut(new PrintStream(outputStream));
    }

    @Test
    void givenValue_whenAddForTheFirstTime_thenAcceptedAsRootNode(){

        int expected = 10 ;
        binaryTree.add(10);
        assertEquals(expected , binaryTree.getRootNode().getData());
    }

    @Test
    void givenValueLargerThanRootNode_whenAdd_thenAcceptedAsRightSideOfRootNode(){
        int expected = 18 ;
        binaryTree.add(10);
        binaryTree.add(18);
        assertEquals(expected ,binaryTree.getRootNode().getRight().getData());
    }

    @Test
    void givenValueLessThanRootNode_whenAdd_thenAcceptedAsLeftSideOfRootNode(){
        int expected = 6 ;
        binaryTree.add(10);
        binaryTree.add(18);
        binaryTree.add(6);
        assertEquals(expected,binaryTree.getRootNode().getLeft().getData());
    }

    @Test
    void givenValueLargerThanRightNode_whenAdd_thenAcceptedAsRightSideOfRightNode(){
        int expected = 21 ;
        binaryTree.add(10);
        binaryTree.add(18);
        binaryTree.add(6);
        binaryTree.add(21);
        assertEquals(expected,binaryTree.getRootNode().getRight().getRight().getData());
    }

    @Test
    void givenValueLessThanRightNodeAndLargerThanLeftNode_whenAdd_thenAcceptedAsLeftSideOfRightNode(){
        int expected = 15 ;
        binaryTree.add(10);
        binaryTree.add(18);
        binaryTree.add(6);
        binaryTree.add(21);
        binaryTree.add(15);
        assertEquals(expected,binaryTree.getRootNode().getRight().getLeft().getData());
    }

    @Test
    void givenValueLargerThanLeftNode_whenAdd_thenAcceptedAsRightSideOfLeftNode(){
        int expected = 8 ;
        binaryTree.add(10);
        binaryTree.add(18);
        binaryTree.add(6);
        binaryTree.add(21);
        binaryTree.add(15);
        binaryTree.add(8);
        assertEquals(expected,binaryTree.getRootNode().getLeft().getRight().getData());
    }

    @Test
    void givenValueLessThanLeftNode_whenAdd_thenAcceptedAsLeftSideOfLeftNode(){
        int expected = 4 ;
        generateBinaryTree();
        assertEquals(expected,binaryTree.getRootNode().getLeft().getLeft().getData());
    }

    @Test
    void givenExistedValue_whenAdd_thenPrintOutNumberIsAlreadyExist(){
        String expected = "the rootNode is: 10\nright of 10 is 18\nleft of 10 is 6\n6 is already exist" ;
        binaryTree.add(10);
        binaryTree.add(18);
        binaryTree.add(6);
        binaryTree.add(6);
        assertEquals(expected,outputStream.toString().trim());
    }

    @Test
    void givenBinaryTree_whenDepthTree_thenReturnTheDepthOfBinaryTree(){
        int expected = 3 ;
        generateBinaryTree();
        assertEquals(expected , binaryTree.treeDepth());
    }

    @Test
    void givenBinaryTree_whenFindDepthForValue_thenReturnTheDepthOfTheValue(){
        int expected = 2 ;
        generateBinaryTree();
        assertEquals(expected , binaryTree.depth(4));
    }

    private void generateBinaryTree(){
        binaryTree.add(10);
        binaryTree.add(18);
        binaryTree.add(6);
        binaryTree.add(21);
        binaryTree.add(15);
        binaryTree.add(8);
        binaryTree.add(4);
    }

}

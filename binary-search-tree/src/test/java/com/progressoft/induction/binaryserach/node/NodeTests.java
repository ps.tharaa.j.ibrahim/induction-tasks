package com.progressoft.induction.binaryserach.node;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class NodeTests {

    private Node node ;
    private final int data = 10  ;

    @BeforeEach
    void setUp(){
         node = new Node(data) ;
    }

    @Test
    void givenFirstNode_whenGetRightAndLeftNode_thenReturnNull(){
        String expected = null ;
        assertEquals(expected , node.getRight()) ;
        assertEquals(expected , node.getLeft());

    }
    @Test
    void givenRightNode_whenGetLeftAndRightNodes_thenReturnLeftNull(){
        String expectedLeftNode = null ;
        int rightNode = 20 ;
        node.accept(rightNode);
        assertEquals(expectedLeftNode , node.getLeft()) ;
        assertEquals(rightNode , node.getRight().getData());
    }
    @Test
    void givenLeftNode_whenGetLeftAndRightNodes_thenReturnRightNull(){
        String expectedRightNode = null ;
        int leftNode = 5 ;
        node.accept(leftNode);
        assertEquals(expectedRightNode , node.getRight()) ;
        assertEquals(leftNode , node.getLeft().getData());
    }

    @Test
    void givenLeftAndRightNodes_whenGetLeftAndRightNodes_thenReturnTheseValues(){
        int leftNode = 5 ;
        int rightNode = 20 ;
        node.accept(leftNode);
        node.accept(20);
        assertEquals(rightNode , node.getRight().getData()) ;
        assertEquals(leftNode , node.getLeft().getData());
    }


}

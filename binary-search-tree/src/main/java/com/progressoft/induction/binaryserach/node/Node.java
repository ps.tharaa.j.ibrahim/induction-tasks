package com.progressoft.induction.binaryserach.node;

public class Node{

    private final int data ;
    private Node right = null ;
    private Node left = null ;


    public Node(int data) {
        this.data = data ;
    }
    public boolean accept(int value) {

        if (value > this.data)
            return addToRight(value);
        else if (value < this.data)
            return addToLeft(value);

        return false ;

    }

    private boolean addToRight(int value){
        if (this.right == null){
            this.right = new Node(value);
            System.out.println( "right" + " of "+ data + " is " + value);
            return true;
        }
        else if (value != this.right.data) return this.right.accept(value) ;
         return false ;
    }

    private boolean addToLeft(int value){
        if (this.left == null) {
            this.left = new Node(value);
            System.out.println( "left" + " of "+ data + " is " + value);
            return true;
        }
        else if (value != this.left.data) return this.left.accept(value);
         return false ;
    }
    public Node getRight() {
        return right;
    }

    public Node getLeft() {
        return left;
    }

    public int getData() {
        return data;
    }

}

package com.progressoft.induction.binaryserach.binaryTree;

import com.progressoft.induction.binaryserach.node.Node;

public interface BinaryTree {

    void add (int data) ;
    boolean accept (int data) ;

    int depth (int data) ;

    int treeDepth () ;

    Node getRootNode() ;
}

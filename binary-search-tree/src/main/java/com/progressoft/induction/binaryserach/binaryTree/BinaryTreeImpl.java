package com.progressoft.induction.binaryserach.binaryTree;
import com.progressoft.induction.binaryserach.node.Node;

public class BinaryTreeImpl implements BinaryTree {
    private Node rootNode ;


    @Override
    public void add(int value) {
        if (!accept(value)) System.out.println(value + " is already exist");
    }

    @Override
    public boolean accept(int value) {
        if(isEmpty(rootNode)){
            rootNode = new Node(value) ;
            System.out.println("the rootNode is: " + value);
            return true ;
        }
            return rootNode.accept(value) ;

    }


    @Override
    public int depth(int data) {
        return getDepthOfValue(rootNode , data);
    }

    @Override
    public Node getRootNode() {
        return rootNode;
    }

    @Override
    public int treeDepth() {
        return getDepthOfRootNode(rootNode) ;
    }

    private int getDepthOfRootNode(Node rootNode) {
        if(isEmpty(rootNode)) return 0 ;
        int left = getDepthOfRootNode(rootNode.getLeft()) ;
        int right = getDepthOfRootNode(rootNode.getRight());
        return Math.max(left , right) + 1 ;
    }

    private int getDepthOfValue(Node rootNode , int value) {
        if (isEmpty(rootNode)) return -1;
        int depth = -1;
        if ((rootNode.getData() == value)
                || (depth = getDepthOfValue(rootNode.getLeft(), value)) >= 0
                || (depth = getDepthOfValue(rootNode.getRight(), value)) >= 0)
             return depth + 1;

        return depth;
    }
    private boolean isEmpty(Node rootNode) {
        return rootNode == null;
    }

}

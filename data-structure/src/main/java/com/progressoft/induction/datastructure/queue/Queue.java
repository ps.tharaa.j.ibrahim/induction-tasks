package com.progressoft.induction.datastructure.queue;
import com.progressoft.induction.datastructure.exceptions.QueueException;
import com.progressoft.induction.datastructure.exceptions.ExceptionMessages;
import com.progressoft.induction.datastructure.generalStructure.GeneralStructureImpl;

public abstract class Queue  {

    private final GeneralStructureImpl queueStructure = new GeneralStructureImpl() ;
    private String[] queue = new String[queueStructure.capacity] ;

    public abstract void enque(String input) throws QueueException;

    public void deque() {
        if(queueStructure.isEmpty())  throw new QueueException.EmptyQueue(ExceptionMessages.EMPTY_QUEUE_MSG) ;
        int newQueueCapacity = queueStructure.capacity;
        String[] newQueue = doDeque(queue , newQueueCapacity);
        queue = newQueue ;
        queueStructure.top-- ;
    }

    public int size() {
        return queueStructure.top;
    }

    public String peek() {
        if (queueStructure.isEmpty())  throw new QueueException.EmptyQueue(ExceptionMessages.EMPTY_QUEUE_MSG) ;
         return queue[queueStructure.head];
    }

    private String[] doDeque(String [] structure, int newCapacity) {

        String[] newQueue = new String[newCapacity];

        int newQueueIndex = 0 ;
        for (int index = 1; index < queueStructure.top ; index ++) {
            newQueue[newQueueIndex] = structure[index] ;
            newQueueIndex ++ ;
        }
        return newQueue ;
    }

    public String[] getQueue() {
        return queue;
    }

    public void setQueue(String[] queue) {
        this.queue = queue;
    }

    public GeneralStructureImpl getQueueStructure() {
        return queueStructure;
    }
}

package com.progressoft.induction.datastructure.generalStructure;

public interface GeneralStructure {

    String[] addElement( String [] structure , String input)  ;

    boolean isFull() ;

    boolean isEmpty( ) ;


}

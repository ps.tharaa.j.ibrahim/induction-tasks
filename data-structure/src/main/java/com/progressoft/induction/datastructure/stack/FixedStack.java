package com.progressoft.induction.datastructure.stack;
import com.progressoft.induction.datastructure.exceptions.StackException;
import com.progressoft.induction.datastructure.exceptions.ExceptionMessages;


public class FixedStack extends Stack {

    public FixedStack(int capacity) {

        this.setStack(new String[capacity]);
        getStackStructure().capacity = capacity ;
    }

    public void push(String input){

        if(getStackStructure().isFull()){
            throw new StackException.OverFlowStack(ExceptionMessages.FULL_STACK_MSG);
        }
        this.getStack()[getStackStructure().top] = input ;

        getStackStructure().top++ ;
    }




}

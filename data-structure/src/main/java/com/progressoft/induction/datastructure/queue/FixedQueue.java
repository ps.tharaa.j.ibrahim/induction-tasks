package com.progressoft.induction.datastructure.queue;
import com.progressoft.induction.datastructure.exceptions.QueueException;
import com.progressoft.induction.datastructure.exceptions.ExceptionMessages;

public class FixedQueue extends Queue {


    public FixedQueue(int capacity) {
        getQueueStructure().capacity = capacity;
        this.setQueue(new String[capacity]);
    }

    @Override
    public void enque(String input) throws QueueException {

        if (getQueueStructure().isFull()) throw new QueueException.OverFlowQueue(ExceptionMessages.FULL_QUEUE_MSG);
        this.getQueue()[getQueueStructure().top] = input;
        getQueueStructure().top++;
    }


}

package com.progressoft.induction.datastructure.exceptions;

public class QueueException extends RuntimeException {

    public QueueException(String message) {
        super(message);
    }

    public static class OverFlowQueue extends RuntimeException{
        public OverFlowQueue(String message) {
            super(message);
        }
    }

    public static class EmptyQueue extends RuntimeException{
        public EmptyQueue(String message) {
            super(message);
        }
    }
}

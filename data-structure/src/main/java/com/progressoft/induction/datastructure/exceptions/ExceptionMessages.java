package com.progressoft.induction.datastructure.exceptions;

public class ExceptionMessages {

    public final static String EMPTY_QUEUE_MSG = "the queue is empty" ;

    public final static String FULL_QUEUE_MSG = "the queue is full" ;

    public final static String EMPTY_STACK_MSG = "the stack is empty" ;

    public final static String FULL_STACK_MSG = "the stack is full" ;
}

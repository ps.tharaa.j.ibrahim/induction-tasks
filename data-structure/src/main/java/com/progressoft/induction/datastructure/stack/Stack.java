package com.progressoft.induction.datastructure.stack;
import com.progressoft.induction.datastructure.exceptions.StackException;
import com.progressoft.induction.datastructure.exceptions.ExceptionMessages;
import com.progressoft.induction.datastructure.generalStructure.GeneralStructureImpl;

public abstract class Stack  {
    
    private final GeneralStructureImpl stackStructure = new GeneralStructureImpl() ;
    private String[] stack = new String[stackStructure.capacity] ;

    public abstract void push(String input) throws StackException;

    public void pop() {
        if (stackStructure.isEmpty()) throw new StackException.EmptyStack(ExceptionMessages.EMPTY_STACK_MSG) ;;
        String [] newStack = doPop(stack , stackStructure.capacity) ;
        stack = newStack ;
        stackStructure.top--;
    }

    public String peek() {

        if (stackStructure.isEmpty())  throw new StackException.EmptyStack(ExceptionMessages.EMPTY_STACK_MSG) ;

        return stack[stackStructure.top -1];
    }

    public int size() {
        return stackStructure.top;
    }

    private String[] doPop(String[] structure, int newCapacity) {
        String [] newStack = new String[newCapacity];

        for (int index = 0; index < stackStructure.top -1 ; index ++){
            newStack[index] = structure[index] ;
        }

        return newStack ;
    }

    public String[] getStack() {
        return stack;
    }

    public void setStack(String[] stack) {
        this.stack = stack;
    }

    public GeneralStructureImpl getStackStructure() {
        return stackStructure;
    }
}

package com.progressoft.induction.datastructure.generalStructure;



public class GeneralStructureImpl implements GeneralStructure{
     public int capacity = 0 ;

     public int top = 0 ;

     public final int head = 0 ;


    @Override
    public String[] addElement(String [] structure  , String input) {
        String[] newStructure = new String[++capacity];

        if(structure.length != 0){
            for(int index = 0 ; index < capacity - 1 ; index++){
                newStructure[index] = structure[index] ;
                newStructure[capacity -1] = input ;
            }
        }else newStructure[0] = input ;

        return newStructure ;
    }

    @Override
    public boolean isFull() {
        return top >= capacity;
    }

    @Override
    public boolean isEmpty() {
        return  (top == 0)  ;
    }





}

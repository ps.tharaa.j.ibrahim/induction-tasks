package com.progressoft.induction.datastructure.exceptions;

public class StackException extends RuntimeException{

    public StackException(String message) {
        super(message);
    }
    public static class OverFlowStack extends RuntimeException{
        public OverFlowStack(String message) {
            super(message);
        }
    }

    public static class EmptyStack extends RuntimeException{
        public EmptyStack(String message) {
            super(message);
        }
    }
}

package com.progressoft.induction.datastructure.stack;

import com.progressoft.induction.datastructure.exceptions.StackException;
import com.progressoft.induction.datastructure.exceptions.ExceptionMessages;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class DynamicStackTests {

    private Stack stack ;

    @BeforeEach
    void setUp(){
        stack = new DynamicStack() ;
    }

    @Test
    void givenDynamicStack_whenPush_thenAddElementToTheTopOfTheStack(){
        String [] expectedStack = {"tharaa" , "jamil" , "harb"} ;

        generateStack();

        assertArrayEquals(expectedStack , stack.getStack()) ;
    }

    @Test
    void givenDynamicStack_whenPop_thenPopElementFromTheStackTop(){
        String [] expectedStack = {"tharaa" , "jamil"} ;

        generateStack();

        stack.pop();

        assertArrayEquals(expectedStack , stack.getStack());

    }

    @Test
    void givenDynamicEmptyStack_whenPop_thenThrowsArrayIndexOutOfBoundsException(){

        String expectedMsg =  ExceptionMessages.EMPTY_STACK_MSG;
        Stack emptyStack = new DynamicStack() ;

        StackException.EmptyStack exception = assertThrows(StackException.EmptyStack.class , () -> emptyStack.pop()) ;
        assertEquals(expectedMsg , exception.getMessage() );

    }

    @Test
    void givenDynamicStack_whenSize_thenReturnTheCurrentNumberOfElementsInTheStack(){
        int expectedSize = 3;

        generateStack();

        int actualSize = stack.size() ;

        assertEquals(expectedSize , actualSize);

    }

    @Test
    void givenDynamicSize_whenPeek_thenReturnReturnTheElementAtTheTopOfTheStack(){

        String expected = "harb" ;

        generateStack();

        assertEquals(expected , stack.peek());
    }

    @Test
    void givenEmptyDynamicStack_whenPeek_thenThrowsArrayIndexOutOfBoundsException(){

        String expectedMsg =  ExceptionMessages.EMPTY_STACK_MSG;
        Stack emptyStack = new DynamicStack() ;

        StackException.EmptyStack exception = assertThrows(StackException.EmptyStack.class , () -> emptyStack.peek()) ;
        assertEquals(expectedMsg , exception.getMessage() );

    }
    private void generateStack(){

        stack.push("tharaa");
        stack.push("jamil");
        stack.push("harb");

    }
}

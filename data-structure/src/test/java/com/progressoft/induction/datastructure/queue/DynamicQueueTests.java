package com.progressoft.induction.datastructure.queue;

import com.progressoft.induction.datastructure.exceptions.QueueException;
import com.progressoft.induction.datastructure.exceptions.ExceptionMessages;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


public class DynamicQueueTests {
    private Queue queue ;

    @BeforeEach
    void setUp(){
        queue = new DynamicQueue() ;
    }

    @Test
    void givenDynamicQueue_whenEnqueValid_thenAddElementToTheReadOfTheQueue() throws QueueException {
        String [] expectedQueue = {"tharaa" , "jamil" , "harb"} ;

        generateQueue();

        assertArrayEquals(expectedQueue , queue.getQueue()) ;
    }

    @Test
    void givenDynamicQueue_whenDeque_thenRemoveElementsFromTheHeadOfTheQueue() throws QueueException {
        String [] expectedQueue = {"jamil" , "harb"} ;

        generateQueue();

        queue.deque();

        assertArrayEquals(expectedQueue , queue.getQueue());

    }

    @Test
    void givenDynamicEmptyQueue_whenDeque_thenThrowsArrayIndexOutOfBoundsException(){

        String expectedMsg = ExceptionMessages.EMPTY_QUEUE_MSG;

        QueueException.EmptyQueue exception = assertThrows(QueueException.EmptyQueue.class , () -> queue.deque()) ;
        assertEquals(expectedMsg , exception.getMessage() );

    }
    @Test
    void givenDynamicQueue_whenSize_thenReturnTheSizeOfQueue() throws QueueException {
        int expectedSize = 3 ;
        generateQueue();

        int actualSize = queue.size();

        assertEquals(expectedSize , actualSize);

    }

    @Test
    void givenDynamicQueue_whenPeek_thenReturnTheElementAtTheHeadOfTheQueue() throws QueueException {

        String expected = "tharaa" ;

        generateQueue();

        assertEquals(expected , queue.peek());
    }

    @Test
    void givenDynamicEmptyQueue_whenPeek_thenThrowsArrayIndexOutOfBoundsException(){

        String expectedMsg = ExceptionMessages.EMPTY_QUEUE_MSG;
        Queue emptyQueue = new DynamicQueue() ;

        QueueException.EmptyQueue exception = assertThrows(QueueException.EmptyQueue.class , () -> emptyQueue.peek()) ;
        assertEquals(expectedMsg , exception.getMessage() );

    }

    private void generateQueue() throws QueueException {
        queue.enque("tharaa");
        queue.enque("jamil");
        queue.enque("harb");
    }
}

package com.progressoft.induction.datastructure.queue;
import com.progressoft.induction.datastructure.exceptions.QueueException;
import com.progressoft.induction.datastructure.exceptions.ExceptionMessages;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class FixedQueueTests {

    private Queue queue ;
    private int size = 3;

    @BeforeEach
    void setUp(){
        queue = new FixedQueue(size) ;
    }

    @Test
    void givenSizeOfTheQueue_whenEnqueValidNumberOfIndexes_thenAddElementToTheReadOfTheQueue() throws QueueException {

        String [] expectedStack = {"tharaa" , "jamil" , "harb"} ;
        generateQueue();

        assertArrayEquals(expectedStack , queue.getQueue()) ;
    }

    @Test
    void givenSizeOfTheQueue_whenEnqueIndexLargerThanQueueSize_thenThrowsArrayIndexOutOfBoundsException() {

        size = 3 ;
        String expectedMsg = ExceptionMessages.FULL_QUEUE_MSG;
        generateQueue();

        QueueException.OverFlowQueue exception = assertThrows(QueueException.OverFlowQueue.class , () -> queue.enque("tharaa")) ;
        assertEquals(expectedMsg , exception.getMessage() );


    }

    @Test
    void givenSizeOfTheQueue_whenDeque_thenRemoveElementsFromTheHeadOfTheQueue()  {


        String [] expectedQueue = {"jamil" , "harb" , null} ;
        generateQueue();
        queue.deque();

        assertArrayEquals(expectedQueue , queue.getQueue());

    }

    @Test
    void givenFixedEmptyQueue_whenDeque_thenThrowsArrayIndexOutOfBoundsException(){


        String expectedMsg = ExceptionMessages.EMPTY_QUEUE_MSG;
        Queue emptyQueue = new FixedQueue(size) ;

        QueueException.EmptyQueue exception = assertThrows(QueueException.EmptyQueue.class , () -> emptyQueue.deque()) ;
        assertEquals(expectedMsg , exception.getMessage() );

    }

    @Test
    void givenSizeOfQueue_whenSize_thenReturnTheCurrentNumberOfElementsInTheQueue() throws QueueException {
       int expectedSize = 3;
       generateQueue(); ;
       int actualSize = queue.size();

       assertEquals(expectedSize , actualSize);

    }

    @Test
    void givenSizeOfQueue_whenPeek_thenReturnTheElementAtTheHeadOfTheQueue()  {

        String expected = "tharaa" ;

        generateQueue();

        assertEquals(expected , queue.peek());
    }

    @Test
    void givenFixedEmptyQueue_whenPeek_thenThrowsArrayIndexOutOfBoundsException(){

        String expectedMsg = ExceptionMessages.EMPTY_QUEUE_MSG;

        Queue emptyQueue = new FixedQueue(3) ;

        QueueException.EmptyQueue exception = assertThrows(QueueException.EmptyQueue.class , () -> emptyQueue.peek()) ;
        assertEquals(expectedMsg , exception.getMessage() );

    }


    private void generateQueue() {
        queue.enque("tharaa");
        queue.enque("jamil");
        queue.enque("harb");
    }



}

package com.progressoft.induction.datastructure.stack;
import com.progressoft.induction.datastructure.exceptions.StackException;
import com.progressoft.induction.datastructure.exceptions.ExceptionMessages;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class FixedStackTests {

    private Stack stack ;


    private int size = 3 ;

   @BeforeEach
   void setUp(){
       stack = new FixedStack(size) ;
   }
    @Test
    void givenSizeOfTheStack_whenPushValidNumberOfIndexes_thenAddElementToTheTopOfTheStack(){

        String [] expectedStack = {"tharaa" , "jamil" , "harb"} ;

        generateStack();

        assertArrayEquals(expectedStack , stack.getStack()) ;
    }

    @Test
    void givenSizeOfTheStack_whenPushIndexLargerThanStackSize_thenThrowsArrayIndexOutOfBoundsException(){

        String expectedMsg = ExceptionMessages.FULL_STACK_MSG;

        generateStack();

        StackException.OverFlowStack exception = assertThrows(StackException.OverFlowStack.class , () -> stack.push("tharaa")) ;
        assertEquals(expectedMsg , exception.getMessage() );


    }

    @Test
    void givenSizeOfTheStack_whenPop_thenPopElementFromTheStackTop(){
;
        String [] expectedStack = {"tharaa" , "jamil" , null} ;

        generateStack();

        stack.pop();

        assertArrayEquals(expectedStack , stack.getStack());

    }

    @Test
    void givenEmptyStack_whenPop_thenThrowsArrayIndexOutOfBoundsException(){

        String expectedMsg = ExceptionMessages.EMPTY_STACK_MSG;
        Stack emptyStack = new FixedStack(size) ;

        StackException.EmptyStack exception = assertThrows(StackException.EmptyStack.class , () -> emptyStack.pop()) ;
        assertEquals(expectedMsg , exception.getMessage() );

    }
    @Test
    void givenSizeOfStack_whenSize_thenReturnTheCurrentNumberOfElementsInTheStack(){

        int expectedSize = 3;

        generateStack();

        int actualSize = stack.size();

        assertEquals(expectedSize , actualSize);

    }

    @Test
    void givenSizeOfStack_whenPeek_thenReturnReturnTheElementAtTheTopOfTheStack(){

        String expected = "harb" ;

        generateStack();

        assertEquals(expected , stack.peek());
    }

    @Test
    void givenEmptyStack_whenPeek_thenThrowsArrayIndexOutOfBoundsException(){

        String expectedMsg =  ExceptionMessages.EMPTY_STACK_MSG;
        Stack emptyStack = new FixedStack(size) ;

        StackException.EmptyStack exception = assertThrows(StackException.EmptyStack.class , () -> emptyStack.peek()) ;
        assertEquals(expectedMsg , exception.getMessage() );

    }


    private void generateStack(){


        stack.push("tharaa");
        stack.push("jamil");
        stack.push("harb");

}
}
